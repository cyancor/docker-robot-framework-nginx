FROM ppodgorsek/robot-framework:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"
COPY start.sh /start.sh

RUN chmod +x /start.sh \
    && apk update \
    && apk add nginx \
    && echo "daemon off;" >> /etc/nginx/nginx.conf \
    #&& echo "types_hash_max_size 4096;" >> /etc/nginx/nginx.conf
    && sed -i "s/\$ROBOT_REPORTS_DIR/\$ReportsDir/g" /opt/robotframework/bin/run-tests-in-virtual-screen.sh \
    && rm -rf /usr/share/nginx/html \
RUN mkdir /content
RUN ln -s /content /usr/share/nginx/html

EXPOSE 80

CMD ["/start.sh"]
