#!/bin/bash

if [ "$ReportsDir" == "" ]; then
    ReportsDir="/opt/robotframework/reports"
fi

/usr/sbin/nginx &

/opt/robotframework/bin/run-tests-in-virtual-screen.sh
